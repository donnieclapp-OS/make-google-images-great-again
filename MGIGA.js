// In general, we:
// 1. Wait for the first click on the page, and then:
//    a. Find the three image preview container divs (they're reused as different images are chosen).
//    b. Add a listener to those containers to detect when the user moves their mouse into them
//    c. Construct the [View Image] and [Search by Image] buttons for all 3 containers
//    d. Populate the link hrefs after a short pause to give Google time to load the container with data
// 2. Then, upon the pointer moving into any container div, we update the button links within that div
//    to reflect its current content.
// 3. In addition, any further click updates link hrefs for all three containers (this is possibly overkill).
//
// - Why do we wait for the first click?
//   The actual full-size image isn't loaded into the DOM until the preview container div is expanded. 
//   Waiting for a click is the easiest way to start looking for that content when it actually exists.
//


var theyClicked = 0; 
var containers = [];

console.log("Reminder: The Make Google Images Great Again extension is changing the content of this page with every click, and when certain things are hovered over.");

// wait for a click (as a proxy for detecting when google loads the preview containers)
document.addEventListener("mousedown", function (e) {
    if (theyClicked == 0) {
        identifyContainers();
        addListeners(containers);
        addButtons(containers);
    } 
    // on all subsequent clicks, update buttons to reflect current container contents
    else {
        updateAllButtons(containers);      
    }
    theyClicked++;
});

function identifyContainers() {
    // We're hard coding the class name to find these containers. Unsure if this is wise.
    // This updates the global containers var to contain an HTMLElement (like an array) with 3 divs.
    // Of the 3, only one is ever visible, and it's hard to predict which one will be visible after
    // any given user interaction.
    containers = document.getElementsByClassName('irc_c');
}

// Add listeners so button links are updated when the pointer moves into one of the container divs.
// This is one way we can tell the content has changed: a different container is hovered over, which
// means a different container is visible.
function addListeners(containers) {
    for (let i = 0; i < containers.length; i++) {

        const container = containers[i];
        container.addEventListener("mouseenter", function (e) {
            updateButtons(container);
        });
    };    
}

// Only used for debugging; this puts a visible label in the top left corner of each container
function addContainerLabel(container, containerNumber) {
    
    var containerLabel = document.createElement('span');
    containerLabel.setAttribute('style', 'color: red; position: absolute; top: 1px; left: 1px;');
    containerLabel.setAttribute('class', 'container_label');
    containerNumber = containerNumber + 1; // because container array is zero-based
    containerLabel.appendChild(document.createTextNode('Container ' + containerNumber));
    container.appendChild(containerLabel);
}

// Construct the new buttons and append them to the table of buttons inside each container div
function addButtons(containers) {

    for (let i = 0; i < containers.length; i++) {
        const container = containers[i];
        
        // the table, tr, and td elements don't have classes or ids, so we have to use their closest
        // parent element with a class to grab them.
        var tableRow = container.getElementsByClassName('irc_but_r')[0].firstChild.firstChild;

        var viewCell = tableRow.insertCell(0);
        viewCell.setAttribute('class', 'view_cell');
        var viewLinkNode = document.createElement('a');
        viewLinkNode.setAttribute('class', 'view_link');
        viewLinkNode.setAttribute('target', '_blank');
        viewLinkNode.setAttribute('rel', 'noopener');
        var viewSpanNode = document.createElement('span');
        viewSpanNode.appendChild(document.createTextNode("View Image"));
        viewLinkNode.appendChild(viewSpanNode);
        viewCell.appendChild(viewLinkNode);

        var searchCell = tableRow.insertCell(1);
        searchCell.setAttribute('class', 'search_cell');
        var searchLinkNode = document.createElement('a');
        searchLinkNode.setAttribute('class', 'search_link');
        searchLinkNode.setAttribute('target', '_blank');
        searchLinkNode.setAttribute('rel', 'noopener');
        var searchSpanNode = document.createElement('span');
        searchSpanNode.appendChild(document.createTextNode("Search by Image"));
        searchLinkNode.appendChild(searchSpanNode);
        searchCell.appendChild(searchLinkNode);      
    }
    // after constructing the buttons, we have to wait a tick while Google fetches the content for
    // the preview, because we're fetchthing the URL of the image from that content after it's loaded.
    setTimeout(function() {updateAllButtons(containers)}, 400);
}

// Given a div, find the first child img node with class .irc_mi.
// Then grab its src and use that to construct and update hrefs for the button links.
function updateButtons(container) {
    
    var imgNode = container.getElementsByClassName('irc_mi')[0]; // get the img tag
    var imgSrc = imgNode.getAttribute('src'); // get the img src
    // console.log("imgSrc: " + imgSrc);
    
    var searchHref = 'http://images.google.com/searchbyimage?image_url=' + imgSrc;

    var view_link = container.getElementsByClassName('view_link')[0];
    view_link.setAttribute('href', imgSrc);
    var search_link = container.getElementsByClassName('search_link')[0];
    search_link.setAttribute('href', searchHref);
    
}

// Call updateButtons(div) for each container div.
function updateAllButtons(containers) {
    
    for (let i = 0; i < containers.length; i++) {
        const container = containers[i];
        
        updateButtons(container);
    }

}